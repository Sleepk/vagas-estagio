package br.edu.unifae.vagas.repository;

import br.edu.unifae.vagas.model.Aluno;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface AlunoRepository extends JpaRepository<Aluno, Long> {

    public boolean findByRa(String ra);

}
