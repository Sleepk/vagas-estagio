package br.edu.unifae.vagas.repository;

import br.edu.unifae.vagas.model.Curso;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Carlos H
 */
@Transactional
public interface CursoRepository extends CrudRepository<Curso, Long> {

}
