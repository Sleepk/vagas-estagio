package br.edu.unifae.vagas.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unifae.vagas.model.Role;
import br.edu.unifae.vagas.repository.RoleRepository;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> buscaTodos() {
        return roleRepository.findAll();
    }

    public Role buscaPorId(Long id) {
        return roleRepository.findById(id).get();
    }

    public void save(Role role) throws Exception {
        try {
            Role roleSalvo = roleRepository.save(role);
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
    }

    public Role update(Role funcionario) throws Exception {
        try {
            Optional<Role> findById = roleRepository.findById(funcionario.getId());
            Role roleAtualizado = null;
            if (!findById.get().equals(funcionario)) {
                roleAtualizado = roleRepository.save(funcionario);
            }

            return roleAtualizado;
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
    }

    public void delete(Long id) throws Exception {
        try {
            Role role = buscaPorId(id);
            roleRepository.delete(role);
        } catch (Exception e) {
            throw new Exception();
        }

    }

    public List<Role> findByNome(String nome) {
        return null;
    }
}
