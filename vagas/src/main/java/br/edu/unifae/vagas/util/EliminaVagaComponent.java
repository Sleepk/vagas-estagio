package br.edu.unifae.vagas.util;

import br.edu.unifae.vagas.model.Vaga;
import br.edu.unifae.vagas.repository.VagaRepository;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Carlos H
 */
@Component
@EnableScheduling
public class EliminaVagaComponent {

    @Autowired
    private VagaRepository vagaRepository;

    //@Scheduled(fixedDelay = 1)
    @Scheduled(cron = "0 0 1 * * *") //Utilizar esta anotação quando tudo estiver pronta
    public void removeVagasAntigas() throws Exception {

        List<Vaga> vagas = vagaRepository.findAllByAtivo(true);

        for (Vaga vaga : vagas) {
            long dias = ChronoUnit.DAYS.between(vaga.getCreatedate(), LocalDate.now());
            if (dias > 30) {
                vaga.setAtivo(false);
                vagaRepository.save(vaga);
            }
        }

    }

}
