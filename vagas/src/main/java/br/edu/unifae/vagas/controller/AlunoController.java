package br.edu.unifae.vagas.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unifae.vagas.model.Aluno;
import br.edu.unifae.vagas.model.User;
import br.edu.unifae.vagas.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.persistence.PersistenceException;

/**
 * @author Julio Cesar
 */
@RestController
@RequestMapping(path = "/api/alunos")
@Api(description = "API REST ALUNOS", tags = {"Aluno Controller"})
@CrossOrigin(origins = "*")
public class AlunoController {

    @Autowired
    private UserService userService;
    
    @GetMapping("/list")
    @ResponseBody
    @ApiOperation(
            value = "Retorna uma lista de Alunos", 
            notes = "Ao fazer a chamada sera retornado todos alunos registrados na base de dados", code = 300)
    public List<Aluno> buscarTodos() {
        return userService.findAllAlunos();
    }

    @GetMapping(path = "/list/{id}")
    @ResponseBody
    @ApiOperation(
            value = "Retorna um Aluno passando o ID", 
            notes = "Ao fazer a chamada deste item passando um {id} por parametro"
    )
    public Aluno buscaPorID(@PathVariable Long id) {
        return (Aluno) userService.findById(id);
    }

    @PostMapping(path = "/add")
    @ResponseBody
    @ApiOperation(
            value = "Salva o registro de aluno", 
            notes = "Necessario passar no corpo da requisicao os dados do aluno"
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Não Foi possivel salvar o Registro")
    })
    public ResponseEntity<?> save(@RequestBody @Valid Aluno aluno) {
        try {
            userService.create(aluno);
            return ResponseEntity.status(200).body(aluno);
        } catch (PersistenceException  e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @DeleteMapping(path = "/remove/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 204, message = "Nenhum conteúdo"),
        @ApiResponse(code = 500, message = "Nao foi Encontrado este Registro")
    })
    @ApiOperation(
            value = "Remove um registro passando o ID", 
            notes = "Remove o Aluno passando o id respectivo por parametro."
    )
    public void delete(@PathVariable Long id) throws Exception {
        Aluno aluno = (Aluno) userService.findById(id);
        userService.delete(aluno.getId());
    }

    @PostMapping(path = "/update/{idAluno}")
    @ApiOperation(value = "Altera o registro", notes = "Deve ser passado o aluno no corpo da requisição")
    public ResponseEntity<Aluno> update(@PathVariable Long idAluno, @RequestBody @Valid Aluno aluno) throws Exception {
        userService.update(idAluno, aluno);
        return ResponseEntity.ok().body(aluno);
    }

    @GetMapping(path = "/list/nome/{nome}")
    @ApiOperation(value = "Buscar aluno por Nome", notes = "Busca aluno passando o parametro nome")
    public List<User> findByNome(@PathVariable String nome) {
        return userService.findByNome(nome);
    }

}
