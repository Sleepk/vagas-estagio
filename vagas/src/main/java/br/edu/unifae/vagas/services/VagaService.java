package br.edu.unifae.vagas.services;

import br.edu.unifae.vagas.dtos.VagasDTO;
import br.edu.unifae.vagas.model.Curso;
import br.edu.unifae.vagas.model.Vaga;
import br.edu.unifae.vagas.repository.CursoRepository;
import br.edu.unifae.vagas.repository.VagaRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VagaService {

    @Autowired
    private VagaRepository vagaRepository;

    @Autowired
    private CursoRepository cursoRepository;

    public List<Vaga> buscaTodos() {
        return (List<Vaga>) vagaRepository.findAll();
    }

    public List<Vaga> buscarTodosAtivo() {
        return vagaRepository.findByVagasAtivo();
    }

    public Vaga buscaPorId(Long id) {
        return vagaRepository.findById(id).get();
    }

    public boolean existePorId(Long id) {
        return vagaRepository.existsById(id);
    }

    public List<Vaga> buscaPorCursoId(Long id) {
        return vagaRepository.findAllByCursoId(id);
    }

    public Vaga save(VagasDTO vagaDTO) throws Exception {
        try {
            Vaga vaga = preencheVagas(vagaDTO);

            vagaRepository.save(vaga);
            return vaga;
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
    }

    public void delete(Long id) throws Exception {
        try {
            Vaga vaga = buscaPorId(id);
            vagaRepository.delete(vaga);
        } catch (Exception e) {
            throw new Exception();
        }

    }

    public void update(VagasDTO novaVaga) throws Exception {
        try {

            if (existePorId(novaVaga.getId())) {
                Vaga vagaAntiga = buscaPorId(novaVaga.getId());
                preencheVagasParaUpdate(vagaAntiga, novaVaga);

                vagaRepository.save(vagaAntiga);
            } else {
                throw new EntityNotFoundException("Registro com id [" + novaVaga.getId() + "] não encontrado");
            }

        } catch (Exception e) {
            throw new Exception();
        }
    }

    private Vaga preencheVagas(VagasDTO vagaDTO) throws NumberFormatException {
        List<Curso> cursos = new ArrayList<>();
        for (String area : vagaDTO.getArea()) {
            cursos.add(cursoRepository.findById(Long.parseLong(area)).get());
        }
        Vaga vaga = new Vaga();
        vaga.setDescricao(vagaDTO.getDescricao());
        vaga.setEmpresa(vagaDTO.getEmpresa());
        vaga.setTelefone(vagaDTO.getTelefone());
        vaga.setEmail(vagaDTO.getEmail());
        vaga.setRequisitos(vagaDTO.getRequisitos());
        vaga.setTitulo(vagaDTO.getTitulo());
        vaga.setSemestralidademinima(vagaDTO.getSemestralidademinima());
        vaga.setDatainicio(LocalDate.now());
        vaga.setCreatedate(LocalDate.now());
        vaga.setDatafim(vagaDTO.getDatafim());
        vaga.setDivulgar(vagaDTO.getDivulgar());
        vaga.setValorEstagio(vagaDTO.getValorEstagio());
        vaga.setCurso(cursos);
        return vaga;
    }

    private Vaga preencheVagasParaUpdate(Vaga vaga, VagasDTO vagaDTO) throws NumberFormatException {
        List<Curso> cursos = new ArrayList<>();
        for (String area : vagaDTO.getArea()) {
            cursos.add(cursoRepository.findById(Long.parseLong(area)).get());
        }

        vaga.setDescricao(vagaDTO.getDescricao());
        vaga.setEmpresa(vagaDTO.getEmpresa());
        vaga.setTelefone(vagaDTO.getTelefone());
        vaga.setEmail(vagaDTO.getEmail());
        vaga.setRequisitos(vagaDTO.getRequisitos());
        vaga.setTitulo(vagaDTO.getTitulo());
        vaga.setSemestralidademinima(vagaDTO.getSemestralidademinima());
        vaga.setDatainicio(LocalDate.now());
        vaga.setCreatedate(LocalDate.now());
        vaga.setDatafim(vagaDTO.getDatafim());
        vaga.setDivulgar(vagaDTO.getDivulgar());
        vaga.setValorEstagio(vagaDTO.getValorEstagio());
        vaga.setCurso(cursos);

        return vaga;
    }

}
