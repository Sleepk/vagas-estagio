package br.edu.unifae.vagas.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unifae.vagas.model.Role;
import br.edu.unifae.vagas.services.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Julio Cesar
 */
@RestController
@RequestMapping(path = "/api/roles")
@Api(description = "API REST ALUNOS", tags = {"Role Controller"})
@CrossOrigin(origins = "*")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/list")
    @ResponseBody
    @ApiOperation(value = "Retorna uma lista de Roles", notes = "Ao fazer a chamada sera retornado todos roles registrados na base de dados", code = 300)
    public List<Role> buscarTodos() {
        return roleService.buscaTodos();
    }

    @GetMapping(path = "/list/{id}")
    @ResponseBody
    @ApiOperation(value = "Retorna um Role passando o ID", notes = "Ao fazer a chamada deste item passando um {id} por parametro")
    public Role buscaPorID(@PathVariable Long id) {
        return roleService.buscaPorId(id);
    }

    @PostMapping(path = "/add")
    @ResponseBody
    @ApiOperation(value = "Salva o registro de role", notes = "Necessario passar no corpo da requisicao os dados do role")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
			@ApiResponse(code = 400, message = "Não Foi possivel salvar o Registro")})
    public ResponseEntity<Role> save(@RequestBody @Valid Role role) throws Exception {
        roleService.save(role);
        return ResponseEntity.status(200).body(role);
    }

    @DeleteMapping(path = "/remove/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
			@ApiResponse(code = 204, message = "Nenhum conteúdo")
        ,
			@ApiResponse(code = 500, message = "Nao foi Encontrado este Registro")})
    @ApiOperation(value = "Remove um registro passando o ID", notes = "Remove o Role passando o id respectivo por parametro. ")
    public void delete(@PathVariable Long id) throws Exception {
        Role role = roleService.buscaPorId(id);
        roleService.delete(role.getId());
    }

    @PostMapping(path = "/update/{idRole}")
    @ApiOperation(value = "Altera o registro", notes = "Deve ser passado o role no corpo da requisição")
    public ResponseEntity<Role> update(@PathVariable Long idRole, @RequestBody @Valid Role role) throws Exception {
        role.setId(idRole);
        return ResponseEntity.ok().body(roleService.update(role));
    }

    @GetMapping(path = "/list/nome/{nome}")
    @ApiOperation(value = "Buscar role por Nome", notes = "Busca role passando o parametro nome")
    public List<Role> findByNome(@PathVariable String nome) {
        return roleService.findByNome(nome);
    }

}
