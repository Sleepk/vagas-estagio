/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unifae.vagas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import br.edu.unifae.vagas.model.Vaga;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Carlos H
 */
@Transactional
public interface VagaRepository extends CrudRepository<Vaga, Long> {

//    @Override
//    default List<Vaga> findAll() {
//        return this.findAll(Sort.by("datainicio"));
//    }

    @Query("SELECT V FROM VAGA V ORDER BY V.datainicio")
    @Override
    public List<Vaga> findAll();

    public List<Vaga> findAllByAtivo(boolean ativo);

    @Query("SELECT V FROM VAGA V WHERE V.ativo = 1")
    public List<Vaga> findByVagasAtivo();

    public List<Vaga> findAllByCursoId(Long curso);

}
