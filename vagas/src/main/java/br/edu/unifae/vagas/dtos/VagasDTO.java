package br.edu.unifae.vagas.dtos;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Carlos H
 */
public class VagasDTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 255)
    private String descricao;

    @NotNull
    @Size(max = 80, message = "O campo telefone deve ter no máximo 80 caracteres")
    private String empresa;

    @NotNull
    @Size(max = 10, message = "O campo telefone deve ter no máximo 10 caracteres")
    @Pattern(regexp = "[1-9]{2}[1-9][0-9]?[0-9]{7}", message = "Por favor verificar o numero digitado")
    private String telefone;

    @Email
    @NotNull
    @Size(max = 50, message = "O campo email deve ter no máximo 80 caracteres")
    private String email;

    @NotNull
    @Size(max = 255, message = "O campo requisitos deve ter no máximo 255 caracteres")
    private String requisitos;

    @NotNull
    @Size(max = 255, message = "O campo titulo deve ter no máximo 255 caracteres")
    private String titulo;

    @Max(value = 20, message = "O campo titulo deve ter no máximo 20 caracteres")
    private int semestralidademinima;

    @NotNull(message = "Não pode ser nulo")
    private LocalDate datainicio;

    @Column(nullable = true)
    private LocalDate datafim;

    @NotNull(message = "Não pode ser nulo")
    private int divulgar;

    private boolean ativo = true;

    @NotNull(message = "Não pode ser nulo")
    private LocalDate createdate;

    private double valorEstagio;

    @NotNull(message = "Não pode ser nulo")
    @NotEmpty(message = "Não pode ser vazio")
    private String[] area;

    public VagasDTO() {
    }

    public VagasDTO(
            long id,
            String descricao,
            String empresa,
            String telefone,
            String email,
            String requisitos,
            String titulo,
            int semestralidademinima,
            LocalDate datainicio,
            LocalDate datafim,
            int divulgar,
            LocalDate createdate,
            double valorEstagio,
            String[] area
    ) {
        this.id = id;
        this.descricao = descricao;
        this.empresa = empresa;
        this.telefone = telefone;
        this.email = email;
        this.requisitos = requisitos;
        this.titulo = titulo;
        this.semestralidademinima = semestralidademinima;
        this.datainicio = datainicio;
        this.datafim = datafim;
        this.divulgar = divulgar;
        this.createdate = createdate;
        this.valorEstagio = valorEstagio;
        this.area = area;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getSemestralidademinima() {
        return semestralidademinima;
    }

    public void setSemestralidademinima(int semestralidademinima) {
        this.semestralidademinima = semestralidademinima;
    }

    public LocalDate getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(LocalDate datainicio) {
        this.datainicio = datainicio;
    }

    public LocalDate getDatafim() {
        return datafim;
    }

    public void setDatafim(LocalDate datafim) {
        this.datafim = datafim;
    }

    public int getDivulgar() {
        return divulgar;
    }

    public void setDivulgar(int divulgar) {
        this.divulgar = divulgar;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public LocalDate getCreatedate() {
        return createdate;
    }

    public void setCreatedate(LocalDate createdate) {
        this.createdate = createdate;
    }

    public double getValorEstagio() {
        return valorEstagio;
    }

    public void setValorEstagio(double valorEstagio) {
        this.valorEstagio = valorEstagio;
    }

    public String[] getArea() {
        return area;
    }

    public void setArea(String[] area) {
        this.area = area;
    }

}
