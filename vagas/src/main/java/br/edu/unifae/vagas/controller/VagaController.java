package br.edu.unifae.vagas.controller;

import br.edu.unifae.vagas.dtos.VagasDTO;
import br.edu.unifae.vagas.model.Vaga;
import br.edu.unifae.vagas.services.VagaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Carlos Francisco
 */
@RestController
@RequestMapping(path = "/api/vagas")
@Api(description = "API REST VAGAS", tags = {"Vaga Controller"})
@CrossOrigin(origins = "*")
public class VagaController {

    @Autowired
    private VagaService vagaService;

    @GetMapping("/list")
    @ResponseBody
    @ApiOperation(value = "Retorna uma lista de Vagas", notes = "Ao fazer a chamada sera retornado todos vagas registrados na base de dados")
    public List<Vaga> buscarTodos() {
        return vagaService.buscaTodos();
    }

    @GetMapping(path = "/list/{id}")
    @ResponseBody
    @ApiOperation(value = "Retorna uma vaga passando o ID", notes = "Ao fazer a chamada deste item passando um {id} por parâmetro")
    public Vaga buscaPorID(@PathVariable Long id) {
        return vagaService.buscaPorId(id);
    }

    @GetMapping(path = "/list/curso/{id}")
    @ResponseBody
    @ApiOperation(value = "Retorna uma vaga passando o Id do curso", notes = "Ao fazer a chamada deste item passando um {id} por parâmetro")
    public List<Vaga> buscaPorCursoId(@PathVariable Long id) {
        return vagaService.buscaPorCursoId(id);
    }

    @PostMapping(path = "/add")
    @ResponseBody
    @ApiOperation(value = "Salva o registro de vaga", notes = "Necessário passar no corpo da requisição os dados da vaga")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Não foi possível salvar o Registro")})
    public ResponseEntity<Vaga> save(@RequestBody @Valid VagasDTO vaga) throws Exception {
        return ResponseEntity.status(200).body(vagaService.save(vaga));
    }

    @DeleteMapping(path = "/remove/{id}")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 204, message = "Nenhum conteúdo"),
        @ApiResponse(code = 500, message = "Registro não encontrado")})
    @ApiOperation(value = "Remove um registro passando o ID", notes = "Remove a vaga passando o id respectivo por parâmetro. ")
    public void delete(@PathVariable Long id) throws Exception {
        Vaga vaga = vagaService.buscaPorId(id);
        vagaService.delete(vaga.getId());
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Altera o registro", notes = "Deve ser passado a vaga no corpo da requisição")
    public void update(@RequestBody @Valid VagasDTO vagaDTO) throws Exception {
        vagaService.update(vagaDTO);
    }

    @GetMapping("/lista")
    @ResponseBody
    @ApiOperation(value = "Retorna uma lista de Vagas", notes = "Ao fazer a chamada sera retornado todos vagas registrados na base de dados")
    public List<Vaga> buscarTodosAtivo() {
        return vagaService.buscaTodos();
    }
}
