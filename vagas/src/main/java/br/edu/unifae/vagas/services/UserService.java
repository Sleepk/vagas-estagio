package br.edu.unifae.vagas.services;

import br.edu.unifae.vagas.model.Aluno;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.edu.unifae.vagas.model.Role;
import br.edu.unifae.vagas.model.User;
import br.edu.unifae.vagas.repository.AlunoRepository;
import br.edu.unifae.vagas.repository.RoleRepository;
import br.edu.unifae.vagas.repository.UserRepository;
import javax.persistence.PersistenceException;
import org.hibernate.exception.ConstraintViolationException;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AlunoRepository alunoRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, RoleRepository roleRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    public List<Aluno> findAllAlunos() {
        return this.alunoRepository.findAll();
    }

    public User findById(Long id) {
        return this.userRepository.findById(id).get();
    }

    public List<User> findByNome(String name) {
        return userRepository.findByName(name);
    }

    public boolean findByRa(String ra) {
        return alunoRepository.findByRa(ra);
    }

    public User create(User user) throws PersistenceException, ConstraintViolationException {
        // funcao para codificar a senha do usuario.
        user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));

        Role userRole = this.roleRepository.findByName("ADMIN");
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

        return this.userRepository.save(user);
    }

    public Aluno create(Aluno aluno) throws PersistenceException, ConstraintViolationException {
        aluno.setPassword(this.bCryptPasswordEncoder.encode(aluno.getPassword()));

        Role userRole = this.roleRepository.findByName("USER");
        aluno.setRoles(new HashSet<Role>(Arrays.asList(userRole)));

        return this.alunoRepository.save(aluno);
    }

    
    
    public Boolean delete(Long id) {
        User user = findById(id);
        if (user != null) {
            this.userRepository.delete(user);
            return true;
        }
        return null;

    }

    public Boolean update(Long id, User user) {
        User userExists = findById(id);

        if (userExists != null) {
            userExists.setId(user.getId());
            userExists.setName(user.getName());
            userExists.setActive(user.isActive());
            userExists.setLastName(user.getLastName());
            // sempre usar o user do item e nunca do que usuario que retorna para nao ter um
            // encode de uma senha codificada.
            userExists.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
            userExists.setEmail(user.getEmail());

            this.userRepository.save(userExists);

            return true;
        }
        return false;
    }

    public User show(Long id) {
        return findById(id);
    }

}
