package br.edu.unifae.vagas.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.unifae.vagas.model.User;
import br.edu.unifae.vagas.repository.UserRepository;
import br.edu.unifae.vagas.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/api/users")
@Api(description = "API REST USERS", tags = { "Users Controller" })
@CrossOrigin(origins = "*")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository repository;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping(path = "/add")
	@ResponseBody
	@ApiOperation(value = "Salva o registro de um usuário do sistema, seja ele um Aluno ou Funcionário", notes = "Necessario passar no corpo da requisicao os dados do Aluno ou Funcionário")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 400, message = "Não foi possivel criar o registro") })
	public User save(@RequestBody @Valid User user) throws Exception {
		try {
			return userService.create(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping("/listar")
	public List<User> listarTudo() {
		return repository.findAll();
	}

	@PostMapping(path = "/update/{id}")
	@ApiOperation(value = "Altera o registro", notes = "Deve ser passado o user no corpo da requisição")
	public ResponseEntity<Boolean> update(@PathVariable Long id, @RequestBody @Valid User user) throws Exception {
		user.setId(id);
		return ResponseEntity.ok().body(userService.update(id, user));
	}

	@DeleteMapping(path = "/remove/{id}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
			@ApiResponse(code = 204, message = "Nenhum conteúdo"),
			@ApiResponse(code = 500, message = "Nao foi Encontrado este Registro") })
	@ApiOperation(value = "Remove um registro passando o ID", notes = "Remove o User passando o id respectivo por parametro. ")
	public void delete(@PathVariable Long id) throws Exception {
		userService.delete(id);
	}

}
