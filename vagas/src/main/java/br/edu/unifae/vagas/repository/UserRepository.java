package br.edu.unifae.vagas.repository;

import br.edu.unifae.vagas.model.Aluno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.edu.unifae.vagas.model.User;
import java.util.List;
import org.springframework.data.domain.Sort;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Override
    default List<User> findAll() {
        return this.findAll(Sort.by("name"));
    }

    public List<User> findByName(String name);

}
