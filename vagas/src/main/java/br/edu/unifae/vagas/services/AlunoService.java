package br.edu.unifae.vagas.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.unifae.vagas.model.Aluno;
import br.edu.unifae.vagas.repository.AlunoRepository;
import org.hibernate.exception.ConstraintViolationException;

@Service
public class AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    public List<Aluno> buscaTodos() {
        return alunoRepository.findAll();
    }

    public Aluno buscaPorId(Long id) {
        return alunoRepository.findById(id).get();
    }

    public boolean findByRa(String ra) {
        return alunoRepository.findByRa(ra);
    }

    public void save(Aluno aluno) throws Exception {
        boolean existe = findByRa(aluno.getRa());

        if (!existe) {
            alunoRepository.save(aluno);
        } else {
            throw new ConstraintViolationException("Já existe um aluno cadastrado com este RA", null, "RA");
        }
    }

    public Aluno update(Aluno funcionario) throws Exception {
        try {
            Optional<Aluno> findById = alunoRepository.findById(funcionario.getId());
            Aluno alunoAtualizado = null;
            if (!findById.get().equals(funcionario)) {
                alunoAtualizado = alunoRepository.save(funcionario);
            }

            return alunoAtualizado;
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
    }

    public void delete(Long id) throws Exception {
        try {
            Aluno aluno = buscaPorId(id);
            alunoRepository.delete(aluno);
        } catch (Exception e) {
            throw new Exception();
        }

    }

    public List<Aluno> findByNome(String nome) {
        return null;
    }
}
