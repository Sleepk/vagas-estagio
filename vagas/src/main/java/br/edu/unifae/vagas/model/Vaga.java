package br.edu.unifae.vagas.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Carlos H
 */
@Entity(name = "VAGA")
@Table
public class Vaga implements Serializable {

    private static final long serialVersionUID = -5426734862183814823L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 255)
    private String descricao;

    @NotNull
    @Size(max = 80, message = "O campo telefone deve ter no máximo 80 caracteres")
    private String empresa;

    @NotNull
    @Size(max = 10, message = "O campo telefone deve ter no máximo 10 caracteres")
    @Pattern(regexp = "[1-9]{2}[1-9][0-9]?[0-9]{7}", message = "Por favor verificar o numero digitado")
    private String telefone;

    @Email
    @NotNull
    @Size(max = 50, message = "O campo email deve ter no máximo 80 caracteres")
    private String email;

    @NotNull
    @Size(max = 255, message = "O campo requisitos deve ter no máximo 255 caracteres")
    private String requisitos;

    @NotNull
    @Size(max = 255, message = "O campo titulo deve ter no máximo 255 caracteres")
    private String titulo;

    @Max(value = 20, message = "O campo titulo deve ter no máximo 20 caracteres")
    private int semestralidademinima;

    @NotNull
    private LocalDate datainicio;

    @Column(nullable = true)
    private LocalDate datafim;

    @NotNull
    private int divulgar;

    @Column(name = "ativo")
    private boolean ativo = true;

    @NotNull
    private LocalDate createdate;

    private double valorEstagio;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "vaga_curso", joinColumns = @JoinColumn(name = "vaga_id"),
            inverseJoinColumns = @JoinColumn(name = "curso_id"))
    private List<Curso> curso;

    public Vaga() {
    }

    public Vaga(Long id) {
        this.id = id;
    }

    public Vaga(
            long id,
            String descricao,
            String empresa,
            String telefone,
            String email,
            String requisitos,
            String titulo,
            int semestralidademinima,
            LocalDate datainicio,
            LocalDate datafim,
            int divulgar,
            LocalDate createdate,
            double valorEstagio,
            List<Curso> area
    ) {
        this.id = id;
        this.descricao = descricao;
        this.empresa = empresa;
        this.telefone = telefone;
        this.email = email;
        this.requisitos = requisitos;
        this.titulo = titulo;
        this.semestralidademinima = semestralidademinima;
        this.datainicio = datainicio;
        this.datafim = datafim;
        this.divulgar = divulgar;
        this.createdate = createdate;
        this.valorEstagio = valorEstagio;
        this.curso = area;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getSemestralidademinima() {
        return semestralidademinima;
    }

    public void setSemestralidademinima(int semestralidademinima) {
        this.semestralidademinima = semestralidademinima;
    }

    public LocalDate getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(LocalDate datainicio) {
        this.datainicio = datainicio;
    }

    public LocalDate getDatafim() {
        return datafim;
    }

    public void setDatafim(LocalDate datafim) {
        this.datafim = datafim;
    }

    public int getDivulgar() {
        return divulgar;
    }

    public void setDivulgar(int divulgar) {
        this.divulgar = divulgar;
    }

    public LocalDate getCreatedate() {
        return createdate;
    }

    public void setCreatedate(LocalDate createdate) {
        this.createdate = createdate;
    }

    public double getValorEstagio() {
        return valorEstagio;
    }

    public void setValorEstagio(double valorEstagio) {
        this.valorEstagio = valorEstagio;
    }

    public List<Curso> getCurso() {
        return curso;
    }

    public void setCurso(List<Curso> curso) {
        this.curso = curso;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vaga other = (Vaga) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.semestralidademinima != other.semestralidademinima) {
            return false;
        }
        if (this.divulgar != other.divulgar) {
            return false;
        }
        if (Double.doubleToLongBits(this.valorEstagio) != Double.doubleToLongBits(other.valorEstagio)) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.empresa, other.empresa)) {
            return false;
        }
        if (!Objects.equals(this.telefone, other.telefone)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.requisitos, other.requisitos)) {
            return false;
        }
        if (!Objects.equals(this.titulo, other.titulo)) {
            return false;
        }
        if (!Objects.equals(this.datainicio, other.datainicio)) {
            return false;
        }
        if (!Objects.equals(this.datafim, other.datafim)) {
            return false;
        }
        if (!Objects.equals(this.createdate, other.createdate)) {
            return false;
        }
        if (!Objects.equals(this.curso, other.curso)) {
            return false;
        }
        return true;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

}
