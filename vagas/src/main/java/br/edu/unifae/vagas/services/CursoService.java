package br.edu.unifae.vagas.services;

import br.edu.unifae.vagas.model.Curso;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.edu.unifae.vagas.repository.CursoRepository;

/**
 *
 * @author Carlos H
 */
@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    public List<Curso> buscaTodos() {
        return (List<Curso>) cursoRepository.findAll();
    }

    public Curso buscaPorId(Long id) {
        return (Curso) cursoRepository.findById(id).get();
    }

    public void save(Curso area) {
        cursoRepository.save(area);
    }

    public void delete(Long id) throws Exception {
        try {
            Curso area = buscaPorId(id);
            cursoRepository.delete(area);
        } catch (Exception e) {
            throw new Exception();
        }

    }

    public void update(Curso area) throws Exception {
        Optional<Curso> findById = cursoRepository.findById(area.getId());

        if (!findById.get().equals(area)) {
            cursoRepository.save(area);
        }
    }
}
