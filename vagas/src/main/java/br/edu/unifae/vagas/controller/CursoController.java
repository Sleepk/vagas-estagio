package br.edu.unifae.vagas.controller;

import br.edu.unifae.vagas.model.Curso;
import br.edu.unifae.vagas.services.CursoService;
import java.util.List;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Carlos H
 */
@RestController
@RequestMapping(path = "/api/cursos")
@Api(description = "API REST CURSO", tags = {"Curso Controller"})
@CrossOrigin(origins = "*")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @GetMapping(path = "/list")
    @ResponseBody
    @ApiOperation(value = "Retorna uma lista de Funcioanrios", notes = "Ao chamar esta funcao retorna todos cursos")
    public List<Curso> buscaTodos() {
        return cursoService.buscaTodos();
    }

    @GetMapping(path = "/list/{id}")
    @ResponseBody
    @ApiOperation(value = "Retorna um Curso passando o ID", notes = "Ao fazer a chamada deste item passando um {id} por parametro")
    public Curso buscaPorId(@PathVariable Long id) {
        return cursoService.buscaPorId(id);
    }

    @PostMapping(path = "/add")
    @ResponseBody
    @ApiOperation(value = "Salva o registro de curso", notes = "Necessario passar no corpo da requisicao os dados do curso")
    public ResponseEntity<Curso> save(@RequestBody @Valid Curso curso) {
        cursoService.save(curso);
        return ResponseEntity.ok().body(curso);
    }

    @DeleteMapping(path = "/remove/{id}")
    @ApiOperation(value = "Remove um registro passando o ID", notes = "Remove o curso passando o respectivo id por parametro. ")
    public void delete(@PathVariable Long id) throws Exception {
        cursoService.delete(id);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Altera o registro", notes = "Deve ser passado o curso no corpo da requisição")
    public void update(@RequestBody @Valid Curso curso) throws Exception {
        cursoService.update(curso);
    }

}
