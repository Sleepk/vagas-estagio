// CARREGAMENTO DE CURSOS
$(function () {

    $.ajax({
        type: 'GET',
        url: 'http://localhost:9090/api/cursos/list',
        contentType: 'application/json',
        success: function (res) {
            for (let i = 0; i < res.length; i++) {
                $('<option value="' + res[i].id + '">' + res[i].nome + '</option>').appendTo('select');
            }
        }, error: function () {
            limpaFiltro();
            $("#lista_vagas").append('<div class="row">Nenhum registro encontrado</div>');
        }
    });

});

// CARREGAMENTO INICIAL DE VAGAS
$(function () {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:9090/api/vagas/list',
        contentType: 'application/json',
        success: function (vagas) {
            for (i = 0; i < vagas.length; i++) {
                $("#vagas").append(
                    '<div class="panel panel-default">'
                    + '<div class="panel-heading">'
                    + 'Nome da vaga' + vagas[i].titulo + '</div>'
                    + '<div class="panel-body">'
                    + 'Descricao:' + vagas[i].descricao
                    + '<a style="float: right" data-toggle="collapse" data-target="#vaga' + i + '">Mostrar mais</a>'
                    + '<div id="vaga' + i + '" class="collapse">'
                    + '<div>Empresa: ' + vagas[i].empresa + '</div>'
                    + '<div>Contato: <a href="mailto:' + vagas[i].email + '">' + vagas[i].email + '</a> | TELEFONE' + vagas[i].telefone + '</div>'
                    + '<div>Requisitos: ' + vagas[i].requisitos + '</div>'
                    + '<div>Semestre Requerido:' + vagas[i].semestralidademinima + '</div>'
                    + '<div>Disponível em: ' + vagas[i].datainicio + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</div>')
            }
        }, error: function () {
            limpaFiltro();
            $("#vagas").append('<div class="row">Nenhum registro encontrado</div>');
        }
    });
});

function limpaFiltro() {
    $("#vagas").empty();
}


// FILTRO POR CURSO
$("#lista_vagas").submit(function (event) {
    event.preventDefault();
    idCurso = event.target.elements[0].value;

    $.ajax({
        type: 'GET',
        url: 'http://localhost:9090/api/vagas/list/curso/' + event.target.elements[0].value,
        contentType: 'application/json',
        success: function (vagas) {
            if (vagas.length > 0) {
                for (i = 0; i < vagas.length; i++) {
                    $("#vagas").append(
                        '<div class="panel panel-default">'
                        + '<div class="panel-heading">'
                        + 'Nome da vaga' + vagas[i].titulo + '</div>'
                        + '<div class="panel-body">'
                        + 'Descricao:' + vagas[i].descricao
                        + '<a style="float: right" data-toggle="collapse" data-target="#vaga' + i + '">Mostrar mais</a>'
                        + '<div id="vaga' + i + '" class="collapse">'
                        + '<div>Empresa: ' + vagas[i].empresa + '</div>'
                        + '<div>Contato: <a href="mailto:' + vagas[i].email + '">' + vagas[i].email + '</a> | TELEFONE' + vagas[i].telefone + '</div>'
                        + '<div>Requisitos: ' + vagas[i].requisitos + '</div>'
                        + '<div>Semestre Requerido:' + vagas[i].semestralidademinima + '</div>'
                        + '<div>Disponível em: ' + vagas[i].datainicio + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>')
                }
            } else {
                limpaFiltro();
                $("#vagas").append('<div class="row">Nenhum registro encontrado</div>');
            }
        }, error: function () {
            limpaFiltro();
            $("#vagas").append('<div class="row">Houveram problemas ao conectar com o servidor</div>');
        }
    });
});