// Inicialização

// funcao carregar quando a pagina abrir 
$(function () {
    $(document).ready(function () {

        // FUNÇÃO PARA PREENCHER SELECT DOS CURSOS
        $(function () {
            $.ajax({
                type: 'GET',
                url: 'http://localhost:9090/api/cursos/list',
                contentType: 'application/json',
                success: function (cursos) {
                    for (let i = 0; i < cursos.length; i++) {
                        $("select").append('<option value="' + cursos[i].id + '">' + cursos[i].nome + '</option>')
                    }
                }
                , error: function () {
                    alert("Ocorreu um erro ao Consultar - Tente Novamente.");
                }
            });

        });

        var params = window.location.search.substring(1).split('&').toString();
        var id = params.replace('id=', '');

        $.ajax({
            type: 'GET',
            url: 'http://localhost:9090/api/vagas/list/' + id,
            dataType: 'json',
            success: function (res) {
                $("#empresa").val(res.empresa);
                $("#email").val(res.email);
                $("#descricao").val(res.descricao);
                $("#requisitos").val(res.requisitos);
                $("#semestralidademinima").val(res.semestralidademinima);
                $("#telefone").val(res.telefone);
                $("#valorEstagio").val(res.valorEstagio);
                $("#datainicio").val(res.datainicio);
                $("#datafim").val(res.datafim);
                $("#createdate").val(res.createdate);
                $("#titulo").val(res.titulo);
            }, error: function () {
                alert("Ocorreu um erro ao Consultar - Tente Novamente.");
            }

        });

    });
});


// Save
$(function () {
    $('#editar_vaga').submit(function (e) {
        e.preventDefault()

        var params = window.location.search.substring(1).split('&').toString();
        var id = params.replace('id=', '');
    
        vagaObject = {
            'id': id,
            'datafim': $('#datafim').val() != "null" ? $('#datafim').val().substring(0, 10) : null,
            'datainicio': $('#datainicio').val() != "null" ? $('#datainicio').val().substring(0, 10) : null,
            'email': $('#email').val(),
            'empresa': $('#empresa').val(),
            'descricao': $('#descricao').val(),
            'semestralidademinima': $('#semestralidademinima').val(),
            'telefone': $('#telefone').val(),
            'area': $('#area').val(),
            'titulo': $('#titulo').val(),
            'divulgar': $('#divulgar').val(),
            'createdate': $('#createdate').val() != "null" ? $('#createdate').val().substring(0, 10) : null,
            "requisitos": $('#requisitos').val(),
            "valorEstagio": $('#valorEstagio').val()
        };

        $.ajax({
            type: 'PUT',
            url: 'http://localhost:9090/api/vagas/update',
            data: JSON.stringify(vagaObject),
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (json) {
                console.log(json);
            },
            error: function () {
                window.location = "gerenciar_estagios.php";
            }
        });
    });

});

function Deletar() {
    // funcao carregar quando a pagina abrir 

    var params = window.location.search.substring(1).split('&').toString();
    var id = params.replace('id=', '');

    var resposta = confirm("Deseja remover esse registro?");

    if (resposta == true) {
        $.ajax({
            type: 'DELETE',
            url: 'http://localhost:9090/api/vagas/remove/' + id,
            success: function (data) {
                alert("Deletado com sucesso.");
                window.location = "gerenciar_estagios.php";

            }, error: function () {
                alert("Ocorreu um erro ao Deletar - Tente Novamente.");
            }
        });
    }

}

