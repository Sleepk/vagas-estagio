// FUNÇÃO PARA CADASTRO DE NOVA VAGA
$(function () {
    $('#cadastro_vaga').submit(function (e) {
        e.preventDefault();

        var urlPost = 'http://localhost:9090/api/vagas/add';
        var formulario = {
            'datafim': $('#datafim').val() != "null" ? $('#datafim').val().substring(0, 10) : null,
            'datainicio': $('#datainicio').val() != "null" ? $('#datainicio').val().substring(0, 10) : null,
            'email': $('#email').val(),
            'empresa': $('#empresa').val(),
            'descricao': $('#descricao').val(),
            'semestralidademinima': $('#semestralidademinima').val(),
            'telefone': $('#telefone').val(),
            'area': $('#area').val()    ,
            'titulo': $('#titulo').val(),
            'divulgar': $('#divulgar').val(),
            'createdate': $('#createdate').val() != "null" ? $('#createdate').val().substring(0, 10) : null,
            "requisitos": $('#requisitos').val(),
            "valorEstagio": $('#valorEstagio').val()
        };
        var json = JSON.stringify(formulario);

        $.ajax({
            type: 'POST',
            url: urlPost,
            data: json,
            cache: false,
            dataType: 'json',
            contentType: 'application/json',
            success: function (resposta) {
                alert("Cadastrado com Sucesso" + resposta);
                e.reload();
                window.location = "gerenciar_estagios.php";
            },
            error: function (err) {
                $('#validacoes').show(true);
                $('#validacoes').focus(true);
                $('#validacoes').append(JSON.parse(err.responseText).message);
            }
        });
    });
});

// FUNÇÃO PARA CARREGAR O DETALHAMENTO (TABELA) DE VAGAS
$(function () {
    $('#lista_vagas').submit(function (e) {
        e.preventDefault();
        $('tbody').empty();

        $.ajax({
            type: 'GET',
            url: 'http://localhost:9090/api/vagas/list',
            contentType: 'application/json',
            success: function (vagas) {

                for (let i = 0; i < vagas.length; i++) {
                    var cursos = '';

                    for (let index = 0; index < vagas[i].curso.length; index++) {
                        cursos += vagas[i].curso[index].nome + '<br>';
                    }

                    $("tbody").append('<tr>'
                        + ' <td align="center"><a href="editar_estagios.php?id=' + vagas[i].id + '">' + vagas[i].id + '</a></td>'
                        + '<td align="center"><a href="editar_estagios.php?id=' + vagas[i].id + '">' + cursos + '</a></td>'
                        + '<td align="center"><a href="editar_estagios.php?id=' + vagas[i].id + '">' + vagas[i].titulo + '</a></td>'
                        + '<td align="center"><a href="editar_estagios.php?id=' + vagas[i].id + '">' + vagas[i].empresa + '</a></td>'
                        + '<td align="center"><a href="editar_estagios.php?id=' + vagas[i].id + '">' + vagas[i].email + '</a></td>'
                        + '<td align="center">' + vagas[i].createdate + '</td>'
                        + '<td align="center">' + (vagas[i].ativo ? 'Sim' : 'Não') + '</td> '
                        + '</tr>'
                    )

                }
            }
            , error: function () {
                alert("Ocorreu um erro ao Consultar - Tente Novamente.");
            }
        });

    });
});

// FUNÇÃO PARA PREENCHER SELECT DOS CURSOS
$(function () {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:9090/api/cursos/list',
        contentType: 'application/json',
        success: function (cursos) {
            for (let i = 0; i < cursos.length; i++) {
                $("select").append('<option value="' + cursos[i].id + '">' + cursos[i].nome + '</option>')
            }
        }
        , error: function () {
            alert("Ocorreu um erro ao Consultar - Tente Novamente.");
        }
    });

});
