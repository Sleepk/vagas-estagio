<?php 
	include("header-simples.php")
?>

<!-- BUSCA VAGAS POR CURSO -->
<div class="container">
    <div class="sub-content" style="margin-top:80px">
        <div class="titulo" style="padding-top:50px">
            <img src="../images/detalhe.png" height="20"> ESTÁGIOS DISPONÍVEIS
        </div>
        <form method="post" action="#" id="lista_vagas" enctype="multipart/form-data">
            <div class="form-group col-lg-9">
                <select id="pesquisar" name="pesquisar" class="form-control" onchange="limpaFiltro()">
                    <option value="">Filtrar por curso</option>
                </select>
            </div>
            <div class="form-group col-lg-3">
                <button type="submit" id="enviar" name="enviar" class="button">VAGAS</button>
            </div>
        </form>
    </div>
</div>

<!-- UTILIZADO PARA EXIBIR AS VAGAS -->
<div class="container" id="vagas">

</div>

<?php 
	include("footer.php")
?>    

<script src="../js/scriptEstagiosAlunos.js"></script>

