<?php 
	include("header.php")
?>
	
    <section id="contato">
    <div class="container">
        <div class="titulo" style="padding-top:10%">
            <img src="../images/detalhe.png" height="20"> GERENCIAR ESTÁGIOS
        </div>
        <form method="post" action="#" enctype="multipart/form-data">
            <div class="form-group">Editar estágio<br>
                <br>
                <label for="nome">Nome: </label>
                <input type="text" name="nome" id="nome" class="form-control">
            </div>
            <div class="form-group">
                <label for="area">Áreas: </label>
                <select name="area" id="area" class="form-control">
                    <option value="">Selecione</option>
                    <option value="1" selected>Área 1</option>
                    <option value="2">Área 2</option>
                    <option value="3">Área 3</option>
                </select>
            </div>
            <div class="form-group">
                <label for="empresa">Empresa:</label>
                <input type="text" name="empresa" id="empresa" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">E-mail:</label>
                <input type="text" name="email" id="email" class="form-control">
            </div>
            <br>
            <div class="div-fields" style="float:right">
                <button type="submit" id="enviar" name="enviar" class="button">SALVAR</button>
            </div>
        </form>
    </div>
</section>

<script src="../js/jquery-min.js"></script>
<script>
    $(document).ready(function(){
      $("#filtrar").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#estagio tbody tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    </script>

<?php 
	include("footer.php")
?>    