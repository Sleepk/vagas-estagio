<?php 
	include("header.php")
?>
<script language="Javascript">
function voltar()
{
window.location = "gerenciar_estagios";
}
</script>

<div class="container">
    <div class="titulo" style="padding-top:10%"><img src="../images/detalhe.png" height="20"> EDITAR ESTÁGIOS</div>
    <form style="margin-bottom:10%;padding:5%;border:1px solid #aed920" method="post" id="editar_vaga" enctype="application/json">
       <input type="hidden" id="datafim" name="datafim" value="null">
       <input type="hidden" id="datainicio" name="datainicio" value="2018-10-08">
       <input type="hidden" id="divulgar" name="divulgar" value="1">
       <input type="hidden" id="createdate" name="createdate" value="2018-10-08">
       <div class="form-group"> 
          <label for="titulo">Titulo:</label>
          <input type="text" name="titulo" id="titulo" class="form-control">
       </div>
       <div class="form-group"> 
          <label for="empresa">Empresa:</label>
          <input type="text" name="empresa" id="empresa" class="form-control">
       </div>
       <div class="form-group"> 
          <label for="email">E-mail:</label>
          <input type="text" name="email" id="email" class="form-control">
       </div>
       <div class="form-group"> 
          <label for="descricao">Descrição:</label>
          <input type="text" name="descricao" id="descricao" class="form-control">
       </div>
       <div class="form-group" style="vertical-align:top !important">
          <label for="requisitos">Requisitos:</label>
          <textarea name="requisitos" id="requisitos" class="form-control" rows="6">
          </textarea>
       </div>
       <div class="row">
          <div class="form-group col-lg-6">
             <label for="area">Área: (Segure Ctrl para selecionar mais que um)</label>
             <select multiple class="form-control" id="area" name="area">
            </select>
          </div>
          <div class="form-group col-lg-2">
             <button onClick="location.href='adicionar-area.php'" class="btn button btn-md">Nova Área</button>
          </div>
       </div>
       <div class="row">
        <div class="form-group col-lg-4"> 
            <label for="semestralidademinima">Semestre Desejável:</label>
            <input type="text" name="semestralidademinima" id="semestralidademinima" class="form-control">
        </div>
        <div class="form-group col-lg-4"> 
            <label for="telefone">Telefone:</label>
            <input type="text" name="telefone" id="telefone" class="form-control">
        </div>
        <div class="form-group col-lg-4"> 
            <label for="valorEstagio">Valor Estagio:</label>
            <input type="text" name="valorEstagio" id="valorEstagio" class="form-control">
        </div>
       </div>
       <div class="row">
        <button type="submit" id="enviar" name="enviar" value="Salvar"  style="margin-left:2%" class="btn button btn-md">SALVAR</button>
        <button type="button" onClick="voltar()" name="enviar"  value="voltar"  style="margin-left:2%" class="btn button btn-md">VOLTAR</button>
        <button type="button" onClick="Deletar()" name="enviar"  value="voltar"  style="margin-left:2%" class="btn button btn-md">DELETAR</button>
       </div>
    </form>
 </div>
 
<?php 
	include("footer.php")
?>    

<script src="../js/scriptEditar.js"></script>

