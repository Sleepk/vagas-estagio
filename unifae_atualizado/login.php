<?php 
	include("header.php")
?>
	
<section id="contato">
    <div class="sub-content" style="text-align:center;margin-top:80px">
        <div id="login" class="conteudo-item" style="padding:20px;margin-bottom:50px;margin-top:50px;border:1px solid #aed920">
            <div id="erro">
            </div>
            <div class="titulo" style="padding-top:20px"><img src="images/detalhe.png" height="20">
                LOGIN
            </div>
            <form method="post" id="autenticacao" enctype="multipart/form-data" onsubmit="enviaForm(event)">
                <div class="div-fields"><label for="email">Login: </label><br>
                    <input type="text" id="email" name="email" class="fields" required />
                </div>
                <div style="clear:both;width:100%;height:1px;">
                </div>
                <div class="div-fields"><label for="password">Senha: </label><br>
                    <input type="password" id="password" name="password" class="fields" required />
                </div>
                <div style="clear:both;width:100%;height:1px;">
                </div>
                <div class="div-fields"><label for="tipo">Tipo de usuário: </label><br>
                    <select id="tipo" name="tipo" class="fields" required>
                        <option value="">Selecione</option>
                        <option value="1">Aluno</option>
                        <option value="2">Funcionário</option>
                    </select>
                </div>
                <div style="clear:both;width:100%;height:1px;">
                </div><br>
                <div class="div-fields" style="float:right">
                    <button type="submit" id="enviar" name="enviar" class="button">Entrar</button>
                </div>
            </form>
        </div>
    </div>
</section>


<?php 
	include("footer.php")
?>    

<script src="js/login.js"></script>