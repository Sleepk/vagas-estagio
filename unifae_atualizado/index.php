﻿<?php 
	include("header.php")
?>
	
    <section id="home">
    	<div class="sub-content">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top:120px">                        
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active"> <img src="images/Slide2.jpg" style="width:100%" alt="First slide">
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a></div>
            </div>
        </div>
	</section>
    
    <section id="servicos">
    	<div class="sub-content" style="text-align:center">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> NOSSOS SERVIÇOS</div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
        </div>
    </section>
    
    <section id="noticias">
    	<div class="sub-content">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> NOTÍCIAS</div>
        	<div class="conteudo" style="padding:20px;">
            	<div class="conteudo">
            		<img src="images/noticia1.jpg" width="300">
            	</div>
                <div class="conteudo">
                	<div class="corpo-conteudo">12 de Abril de 2018</div>
            		<div class="titulo-noticia"> Neap da unifae</div>
                    <div class="corpo-conteudo" style="max-width:400px;">Para agilizar os agendamentos e atendimentos do Núcleo de Estudos e Atendimentos em Psicologia (NEAP), estudantes de Engenharia de Software e da Computação da UNIFAE criaram um novo sistema para a clínica que facilita e agiliza as consultas, diminuindo a fila de espera.

 


“Ele vai agregar desde o cadastro de pacientes até os relatórios dos atendimentos, permitindo o cruzamento de informações.
<a>Leia Mais</a></div>
            	</div>
            </div>
            <div class="conteudo" style="padding:20px;">
            	<div class="conteudo">
            		<img src="images/noticia2.jpg" width="300">
            	</div>
                <div class="conteudo">
                	<div class="corpo-conteudo">03 de outubro 2018</div>
            		<div class="titulo-noticia">Unifae Feira de profissoes</div>
                    <div class="corpo-conteudo" style="max-width:400px;">A quadra poliesportiva e as salas de aula do bloco C do Campus estiveram lotadas, na manhã do segundo dia da II Feira de Profissões, nesta quarta-feira (03), quando mais de 1600 pessoas visitaram e conheceram a estrutura montada por representantes dos cursos oferecidos pela UNIFAE. Eles puderam participar de inúmeras dinâmicas junto aos estudantes de ensino superior.<a>Leia Mais</a></div>
            	</div>
            </div>
        </div>
    </section>
    
    <section id="contato">
    	<div class="sub-content" style="text-align:center">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> FALE CONOSCO</div>
            <div class="conteudo" style="padding:20px;padding-bottom:50px;">
                <form method="post" action="#" enctype="multipart/form-data">
                    <div class="div-fields"><label for="nome">Nome: </label><br><input type="text" id="nome" name="nome" class="fields" required/></div>
                    <div class="div-fields"><label for="fone">Telefone: </label><br><input type="text" id="fone" name="fone" class="fields" required/></div>
                    <div style="clear:both;width:100%;height:1px;"></div>
                    <div class="div-fields"><label for="email">E-mail: </label><br><input type="text" id="email" name="email" class="fields" required/></div>
                    <div class="div-fields"><label for="assunto">Assunto: </label><br><input type="text" id="assunto" name="assunto" class="fields" required/></div>
                    <div style="clear:both;width:100%;height:1px;"></div>
                    <div class="div-fields"><label for="mensagem">Mensagem: </label><br><textarea type="text" id="mensagem" name="mensagem" class="textarea" rows="4" maxlength="300" required></textarea></div>
                    <div style="clear:both;width:100%;height:1px;"></div><br>
                    <div class="div-fields" style="float:right"><button type="submit" id="enviar" name="enviar" class="button">Enviar</button></div>
                </form>
            </div>
        </div>
    </section>

<?php 
	include("footer.php")
?>    