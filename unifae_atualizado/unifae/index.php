<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>UNIFAE Jobs</title>
    <link rel='shortcut icon' type='image/x-icon' href='images/icon.png' />
    <link rel="stylesheet" href="css/style.css" />
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" />
</head>

<body>
	<div id="menuHeader">
        <div class="content" id="topMenu" style="background:#0e2971;padding:3px;text-align:right;color:#fff">
            <div class="conteudo" style="padding-bottom:5px;padding-right:10px;padding-top:5px;">        
                <form class="pesquisa" action="php/pesquisar.php" method="post">
                  <input type="text" placeholder="Pesquisar..." id="string" name="string">
                  <button type="submit"><img src="images/lupa.png" height="20"></button>
                </form>
            </div>
        </div>
        <label for="show-menu" class="show-menu">Menu</label>
        <input type="checkbox" id="show-menu" role="button"/>
        <ul class="menu">
            <li class="menulink"><img  onclick="location.href='index.php'" src="images/logo.png"  height="50" style="padding:10px;padding-left:20px;padding-right:20px;cursor:pointer"/></li>
            <li class="menulink"><a href="index.php">Home</a></li>
            <li class="menulink"><a href="#destaques">Nossos Serviços</a></li>
            <li class="menulink"><a href="#destaques">Principais Notícias</a></li>
            <li class="menulink"><a href="#resorts">Fale Conosco</a></li>
            <li class="menulink"><a href="#resorts">Cadastro | Login</a></li>
        </ul>
    </div>
    
    <section id="home">
    	<div class="sub-content">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top:120px">                        
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active"> <img src="images/slide.jpg" style="width:100%" alt="First slide">
                    </div>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a></div>
            </div>
        </div>
	</section>
    
    <section id="servicos">
    	<div class="sub-content" style="text-align:center">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> NOSSOS SERVIÇOS</div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
            <div class="conteudo-item">
            	<img src="images/servico.png" width="200">
            </div>
        </div>
    </section>
    
    <section id="noticias">
    	<div class="sub-content">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> NOTÍCIAS</div>
        	<div class="conteudo" style="padding:20px;">
            	<div class="conteudo">
            		<img src="images/noticia.jpg">
            	</div>
                <div class="conteudo">
                	<div class="corpo-conteudo">24 de Setembro</div>
            		<div class="titulo-noticia"> Título da notícia</div>
                    <div class="corpo-conteudo" style="max-width:400px;">Parte introdutória da notícia, onde depois de 260 caracteres, contando com o espaço, a mesma é substituida por (...) e para visualizar o restante basta clicar no link ao final do texto, conforme vai acontecer aqui e agora, nesse texto de teste para a notícia... <a>Leia Mais</a></div>
            	</div>
            </div>
            <div class="conteudo" style="padding:20px;">
            	<div class="conteudo">
            		<img src="images/noticia.jpg">
            	</div>
                <div class="conteudo">
                	<div class="corpo-conteudo">24 de Setembro</div>
            		<div class="titulo-noticia"> Título da notícia</div>
                    <div class="corpo-conteudo" style="max-width:400px;">Parte introdutória da notícia, onde depois de 260 caracteres, contando com o espaço, a mesma é substituida por (...) e para visualizar o restante basta clicar no link ao final do texto, conforme vai acontecer aqui e agora, nesse texto de teste para a notícia... <a>Leia Mais</a></div>
            	</div>
            </div>
        </div>
    </section>
    
    <section id="contato">
    	<div class="sub-content" style="text-align:center">
        	<div class="titulo"><img src="images/detalhe.png" height="20"> NOSSOS SERVIÇOS</div>
            <div class="conteudo" style="padding:20px;padding-bottom:50px;">
                <form method="post" action="#" enctype="multipart/form-data">
                    <div class="div-fields"><label for="nome">Nome: </label><br><input type="text" id="nome" name="nome" class="fields" required/></div>
                    <div class="div-fields"><label for="fone">Telefone: </label><br><input type="text" id="fone" name="fone" class="fields" required/></div>
                    <div style="clear:both;width:100%;height:1px;"></div>
                    <div class="div-fields"><label for="email">E-mail: </label><br><input type="text" id="email" name="email" class="fields" required/></div>
                    <div class="div-fields"><label for="assunto">Assunto: </label><br><input type="text" id="assunto" name="assunto" class="fields" required/></div>
                    <div style="clear:both;width:100%;height:1px;"></div>
                    <div class="div-fields"><label for="mensagem">Mensagem: </label><br><textarea type="text" id="mensagem" name="mensagem" class="textarea" rows="4" maxlength="300" required></textarea></div>
                    <div style="clear:both;width:100%;height:1px;"></div><br>
                    <div class="div-fields" style="float:right"><button type="submit" id="enviar" name="enviar" class="button">Enviar</button></div>
                </form>
            </div>
        </div>
    </section>
    
    <section>
    	<div class="content" style="background-color:#0e2971">
        	<div class="conteudo-item" style="width:280px;">
            	<br>
            	<img src="images/logo2.png" width="200">
            </div>
            <div class="conteudo-item" style="width:280px;color:#fff">
            	<br>
            	Largo Engenheiro Paulo de<br>
                Almeida Sandeville, 15<br>
				São João da Boa Vista - SP<br>
                <br>
            </div>
            <div class="conteudo-item" style="width:280px;color:#fff">
            	<br>
            	<img src="images/facebook" width="30">
                <img src="images/instagram" width="30">
                <img src="images/twitter" width="30">
                <img src="images/youtube" width="30">
            </div>
        </div>
        <div class="content" style="background-color:#081f5d;padding:5px;">
        	<div class="conteudo" style="padding-top:5px;color:#fff;font-size:10px">CENTRO UNIVERSITÁRIO DAS FACULDADES ASSOCIADAS DE ENSINO - FAE Copyright © 2018 Todos os direitos Reservados</div>
        </div>
    </section>
    
    

</body>
</html>