# Procedimentos para configurar o projeto.
* Necessario criar um novo usuario no sistema, para nao ficar utilizando o usuario root. 
* Procedimento para o carregamento da tarefa no trello

#Procedimento para criar o usuario vagas no banco de dados mysql. 
* CREATE USER 'vagas'@'localhost' IDENTIFIED BY 'password';
* GRANT ALL PRIVILEGES ON vagas . * TO 'vagas'@'localhost';
* FLUSH PRIVILEGES;



#Começaremos utilizar swagger para documentar o webservice.
* Procedimento para chamar o swagger:

* chamar o swagger localhost:9090/swagger-ui.html.

* Neste item havera algumas descricao de fazer as requisições, ele cria exemplos de uso.

* para maiores informações, verificar o video da michele brito (onde usei como base pro projeto).


* https://www.youtube.com/watch?v=HX4lheDqoiA

#Criado branch para desenvolvimento do sistema. uma chamada back-end outra chamada front
* para usar estas branchs devemos ter subido todos os arquivos anteriormente, para nao haver conflitos,
* apos subir os arquivos, commitar, iremos agora chamar os itens da branch desejada.

* para acessar a branch de back-end seguir os seguintes comandos: git checkout back-end
* para acessar a branch de front seguir os seguintes comandos: git checkout front.

* ao realizar um dos procedimentos iremos acessar as branchs alternativas, os demais comandos seguem normalmente.

* para ver as branchs do projeto acesse o comando: git branch (listara as branchs disponiveis)
