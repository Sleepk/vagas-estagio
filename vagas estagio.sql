-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: vagas
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aluno`
--

DROP TABLE IF EXISTS `aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aluno` (
  `idusuario` bigint(20) NOT NULL,
  `bairro` varchar(50) DEFAULT NULL,
  `celular` varchar(11) NOT NULL,
  `cep` varchar(8) NOT NULL,
  `cidade` varchar(80) NOT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `cpf` varchar(11) NOT NULL,
  `nome` varchar(85) NOT NULL,
  `numero` varchar(11) NOT NULL,
  `ra` varchar(6) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `uf` varchar(20) DEFAULT NULL,
  `User_idUser` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idusuario`,`User_idUser`),
  UNIQUE KEY `ra_UNIQUE` (`ra`),
  KEY `fk_aluno_User1_idx` (`User_idUser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aluno`
--

LOCK TABLES `aluno` WRITE;
/*!40000 ALTER TABLE `aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `aluno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencia`
--

DROP TABLE IF EXISTS `experiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencia` (
  `idexperiencia` int(11) NOT NULL,
  `nome empresa` varchar(50) NOT NULL,
  `funcao` varchar(50) DEFAULT 'Não Informado',
  `inicio` date NOT NULL,
  `fim` date NOT NULL,
  `resumo` varchar(150) DEFAULT 'Não Informado',
  `aluno_idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idexperiencia`,`aluno_idusuario`),
  KEY `fk_experiencia_aluno1_idx` (`aluno_idusuario`),
  CONSTRAINT `fk_experiencia_aluno1` FOREIGN KEY (`aluno_idusuario`) REFERENCES `aluno` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencia`
--

LOCK TABLES `experiencia` WRITE;
/*!40000 ALTER TABLE `experiencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `experiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formacao`
--

DROP TABLE IF EXISTS `formacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formacao` (
  `idformacao` int(11) NOT NULL,
  `escola` varchar(45) NOT NULL,
  `tipocurso` enum('Graduacao','Tecnico','Aperfeicoamento','Profissionalizante') NOT NULL,
  `curso` varchar(45) NOT NULL,
  `aluno_idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idformacao`,`aluno_idusuario`),
  KEY `fk_formacao_aluno_idx` (`aluno_idusuario`),
  CONSTRAINT `fk_formacao_aluno` FOREIGN KEY (`aluno_idusuario`) REFERENCES `aluno` (`idusuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formacao`
--

LOCK TABLES `formacao` WRITE;
/*!40000 ALTER TABLE `formacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `formacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcionario` (
  `id` bigint(20) NOT NULL,
  `ra` varchar(8) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `celular` varchar(11) NOT NULL,
  `User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`id`,`User_idUser`),
  UNIQUE KEY `ra_UNIQUE` (`ra`),
  KEY `fk_funcionario_User1_idx` (`User_idUser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionario`
--

LOCK TABLES `funcionario` WRITE;
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `idUser` int(11) NOT NULL,
  `usertype` varchar(20) NOT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vagas`
--

DROP TABLE IF EXISTS `vagas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vagas` (
  `idVaga` int(11) NOT NULL,
  `descricao` varchar(45) DEFAULT 'Não Informado',
  `empresa` varchar(80) NOT NULL,
  `telefone` varchar(20) DEFAULT 'Não Informado',
  `email` varchar(50) NOT NULL,
  `requisitos` varchar(80) NOT NULL,
  `nome` varchar(80) NOT NULL,
  `semestralidademinima` int(11) DEFAULT NULL,
  `datainicio` timestamp NOT NULL,
  `datafim` timestamp NULL DEFAULT NULL,
  `divulgar` tinyint(4) NOT NULL,
  `createdat` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idVaga`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vagas`
--

LOCK TABLES `vagas` WRITE;
/*!40000 ALTER TABLE `vagas` DISABLE KEYS */;
/*!40000 ALTER TABLE `vagas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vagas_has_aluno`
--

DROP TABLE IF EXISTS `vagas_has_aluno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vagas_has_aluno` (
  `Vagas_idVaga` int(11) NOT NULL,
  `aluno_idusuario` bigint(20) NOT NULL,
  `aluno_User_idUser` int(11) NOT NULL,
  PRIMARY KEY (`Vagas_idVaga`,`aluno_idusuario`,`aluno_User_idUser`),
  KEY `fk_Vagas_has_aluno_aluno1_idx` (`aluno_idusuario`,`aluno_User_idUser`),
  KEY `fk_Vagas_has_aluno_Vagas1_idx` (`Vagas_idVaga`),
  CONSTRAINT `fk_Vagas_has_aluno_Vagas1` FOREIGN KEY (`Vagas_idVaga`) REFERENCES `vagas` (`idvaga`),
  CONSTRAINT `fk_Vagas_has_aluno_aluno1` FOREIGN KEY (`aluno_idusuario`, `aluno_User_idUser`) REFERENCES `aluno` (`idusuario`, `user_iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vagas_has_aluno`
--

LOCK TABLES `vagas_has_aluno` WRITE;
/*!40000 ALTER TABLE `vagas_has_aluno` DISABLE KEYS */;
/*!40000 ALTER TABLE `vagas_has_aluno` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-29 12:57:06
